@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

   
@include('common.slider')
    <div class="category-area section-space">
        <div class="container wide">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  section title  =======-->
                    <div class="section-title-wrapper text-center section-space--half">
                        <h4 class="titcus1">CHOOSE YOUR</h4>
                        <h2 class="section-title titcus">CATEGORY</h2>
                       
                    </div>
                    <!--=======  End of section title  =======-->
                </div>

                <div class="col-lg-12">
                    <!--=======  category wrapper  =======-->
                    <div class="category-wrapper">
                        <div class="row row-10 masonry-category-layout--style2">
                            <div class="col grid-item2">
                                <!--=======  single category item  =======-->
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/1.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">MILLS</h3>
                                        
                                    </div>
                                </div>
                                <!--=======  End of single category item  =======-->
                            </div>
                            <div class="col grid-item2">
                                <!--=======  single category item  =======-->
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/2.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">KNITTING</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col grid-item2">
                              
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/3.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">DYEING</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col grid-item2">
                              
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/4.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">PRINITNG</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col grid-item2">
                              
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/5.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">PROCESSING</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>


                            <div class="col grid-item2">
                              
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/6.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">JOB WORK UNIT</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col grid-item2">
                              
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/7.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">FINISHING FABRIC</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            
                            
                            <div class="col grid-item2">
                              
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/8.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">SECOND FABRIC</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col grid-item2">
                              
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/9.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">SECONDS PIECES</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>


                                <div class="col grid-item2">
                              
                                <div class="single-category-item">
                                    <div class="single-category-item__image">
                                        <a href="#">
                                            <img src="{{url('themeassets/img/category/10.jpg')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="single-category-item__content">
                                        <h3 class="title cats">ACCESSORIES</h3>
                                        
                                    </div>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                    <!--=======  End of category wrapper  =======-->
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of category area  ====================-->
    
        <div class="container-fluid benifitmain" style="background-image: url('{{ asset('themeassets/img/backgrounds/bg1.jpg')}}');">
 
  <div class="row">
   
        <div class="col-sm-6" >

        </div>


    
        <div class="col-sm-6 ">
        

           <h2 class="colorred benhead">Benifits</h2>
           <h4 class="colorred">You get working with us</h4>
           <div>
            <div>
          <hr style="width: 100%">
          </div>
      </div>
           <div class="row benifits">
              <div class=" col-sm-6">
               
                <img src="{{url('themeassets/img/icons/1.png')}}">
            
                <span>Timely Deliery can be improved</span>
               

        
               </div>
                  
                  <div class=" col-sm-6">
                <img src="{{url('themeassets/img/icons/2.png')}}">
                       <span>Finding Or Sourcing Vendros made Easy</span>

        
               </div>

               <div class=" col-sm-6">
                <img src="{{url('themeassets/img/icons/3.png')}}">
                       <span>Payment delay can be reduced</span>

        
               </div>


               <div class=" col-sm-6">
                <img src="{{url('themeassets/img/icons/4.png')}}">
                       <span>Reduce dependency</span>

        
               </div>


               <div class=" col-sm-6">
                <img src="{{url('themeassets/img/icons/5.png')}}">
                       <span>Working Capital blokage in terms </span>

        
               </div>


               <div class=" col-sm-6">
                <img src="{{url('themeassets/img/icons/6.png')}}">
                       <span>Reduce commusion by employee </span>

        
               </div>


           </div>


           

            </div>
        </div>
</div>

    <br> <br> <br>
    <!--====================  single row slider ====================-->
    <div class="single-row-slider-area section-space">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  section title  =======-->
                    <div class="section-title-wrapper text-center section-space--half">
                        <h4 class="titcus1">KNOW WHAT</h4>
                        <h2 class="section-title titcus">CUSTOMER SAY</h2>
                       
                    </div>
                    <!--=======  End of section title  =======-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  single row slider wrapper  =======-->
                    <div class="single-row-slider-wrapper">
                        <div class="ht-slick-slider" data-slick-setting='{
                        "slidesToShow": 4,
                        "slidesToScroll": 1,
                        "arrows": true,
                        "autoplay": false,
                        "autoplaySpeed": 5000,
                        "speed": 1000,
                        "infinite": false,
                        "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
                        "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
                    }' data-slick-responsive='[
                        {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                        {"breakpoint":1199, "settings": {"slidesToShow": 4, "arrows": false} },
                        {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": false} },
                        {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                        {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                        {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
                    ]'>

                            <div class="col">
                                <!--=======  single grid product  =======-->
                                <div class="single-grid-product">
                                   
                                    <div class="single-grid-product__content">
                                        

                                        <h3 class="single-grid-product__title"> <a href="#">Cillum dolore lorem ipsum  its decoration item We and our main partners are using technologies cookies and process personal and textile industry</a></h3>
                                        <h3 class="colorred">Contact address</h3>
                                        <p class="colorred">main ideas of textile</p>
                                        <br>
                                    </div>
                                </div>
                                <!--=======  End of single grid product  =======-->
                            </div>

                            <div class="col">
                                <!--=======  single grid product  =======-->
                                <div class="single-grid-product">
                                    
                                    <div class="single-grid-product__content">
                                        

                                        <h3 class="single-grid-product__title"> <a href="#">The sector is highly responsible for propelling India’s overall and  develop development and enjoys intense focus from   it policies and energy of health </a></h3>
                                        <h3 class="colorred">Contact address</h3>
                                        <p class="colorred">main ideas of textile</p>
                                        <br>
                                    </div>
                                </div>
                               
                            </div>

                            <div class="col">
                               
                                <div class="single-grid-product">
                                    
                                    <div class="single-grid-product__content">
                                        
                                        <h3 class="single-grid-product__title"> <a href="#">The sector is highly responsible for propelling India’s overall and good development and enjoys intense focus from Government for initiating </a></h3>
                                       <h3 class="colorred">Contact address</h3>
                                        <p class="colorred">main ideas of textile</p>
                                        <br>
                                    </div>
                                </div>
                               
                            </div>

                            <div class="col">
                                
                                <div class="single-grid-product">
                                   
                                    <div class="single-grid-product__content">
                                       

                                        <h3 class="single-grid-product__title"> <a href="#">The Government for initiating policies that would ensure  creation of world class textile infrastructure in the country and management tech.</a></h3>
                                        <h3 class="colorred">Contact address</h3>
                                        <p class="colorred">main ideas of textile</p>
                                        <br>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col">
                               
                                <div class="single-grid-product">
                                    
                                    <div class="single-grid-product__content">
                                       

                                        <h3 class="single-grid-product__title"> <a href="#">Cillum dolore lorem ipsum decoration item nfrastructure sector includes power, bridges, dams, roads and urban infrastructure development.</a></h3>
                                       <h3 class="colorred">Contact address</h3>
                                        <p class="colorred">main ideas of textile</p>
                                        <br>
                                    </div>
                                </div>
                             
                            </div>

                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="container-fluid">
 
  <div class="row">
   
    <div class="col-sm-6 regimg1" style="background-image: url('{{ asset('themeassets/img/backgrounds/b1.jpg')}}');" ><h4 class="center1">Register your company today &</h4><h5 class="center1">Start your Business</h5>
        
            <form class="form_home" style="display: inline-flex;">
                <div class="col-sm-6">
                  

                 <input type="text" name="sss" value="" class="form-control">
                

                 <input type="text" name="sss" value="" class="form-control">
             </div>
                 <div class="col-sm-6">

                 <input type="text" name="sss" value="" class="form-control">
            

                 <input type="sumit" name="sumit" class="btn btn-danger" value="REGISTER" class="form-control col-sm-3" style="width: 50%;float: left;">
             </div>
            </form>
        </div>


    
    <div class="col-sm-6 regimg2" style="background-image: url('{{ asset('themeassets/img/backgrounds/b21.jpg')}}');">
        

           <h2>The Best business App for you</h2>
           <h3>Textile industry App</h3>
           <h4>Download the App and get  updated</h4>
          
           <div class="row appimg   col-sm-12">
              <div class="appimg1">
                <img src="{{url('themeassets/img/app1.png')}}">


                 <img src="{{url('themeassets/img/app2.png')}}">
               </div>
           </div>
           

    </div>
  </div>
</div>

   
<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12 bootCols">
            <img src="{{url('themeassets/img/icons/i1.png')}}">
            <h3>A textile industry</h3>
            <p>A textile is a flexible material consisting of a network of natural or artificial fibers. primarily concerned with the design, production and distribution </p>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 bootCols">
            <img src="{{url('themeassets/img/icons/i2.png')}}">
            <h3>Best material Collection</h3>
            <p>A textile is a flexible material consisting of a network of natural or artificial fibers primarily concerned with the design, production and distribution</p>
         
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 bootCols">
             <img src="{{url('themeassets/img/icons/i3.png')}}">
            <h3>Nice collection for you</h3>
            <p>A textile is a flexible  material consisting of a network of natural or artificial fibers ,rs primarily concerned with the design, production and distribution</p>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 bootCols">
            <img src="{{url('themeassets/img/icons/i4.png')}}">
            <h3>A fabrics and material</h3>
            <p>A textile is a flexible material ,ssrs primarily concerned with the design, production and distribution  consisting of a network of natural or artificial fibers</p>
        </div>
    </div>
  </div>
  @endsection

@section('front_script')

@endsection