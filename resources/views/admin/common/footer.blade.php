 
       <footer class="footer">
             <div class="container">
              <div class="w-100 clearfix">
                 <div class="row">
                      <div class="col-md-2 grid-margin">
                        <img src="{{url('themeassets/img/logo.png')}}" style="width: 120px;">
                      </div>
                      <div class="col-md-3 grid-margin">
                        <h5>Quick Links</h5>
                        <hr>
                         <div class="row">
                        <ul>
                          <li><a href="#">My Account</a></li>
                          <li><a href="#">About us</a> </li>

                        </ul>
                        <ul>
                          <li><a href="#">My Projects</a></li>
                          <li><a href="#">My Services</a></li>

                        </ul>
                        </div>
                        
                      </div>

                      <div class="col-md-5 grid-margin">
                         <h5>Terms</h5>
                        <hr>
                          <div class="row">
                        <ul>
                          <li><a href="#">Privacy Policy</a></li>
                          <li><a href="#">Terms and Conditions</a></li>

                        </ul>
                        <ul>
                          <li><a href="#">Copyright Policy</a></li>
                          <li><a href="#">Code of Conduct</a></li>

                        </ul>
                        <ul>
                          <li><a href="#">Fees and Changes</a></li>
                          

                        </ul>
                        </div>

                      </div>
                      <div class="col-md-2 grid-margin">
                        <div class="row socialfoot">
                            <a><i class="fa fa-facebook"></i></a>
                              <a><i class="fa fa-twitter"></i></a>
                                <a><i class="fa fa-linkedin"></i></a>
                        </div>
                        <div class="row ">
                        <p style="margin-top: 10px;margin-left: 10px;">© 2020. All rights reserved.</p>
                      </div>
                      </div>
                </div>

     
                </div>

                  
          
              </div>
            </div>
        </footer>

 </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{url('adminassets/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{url('adminassets/js/off-canvas.js')}}"></script>
  <script src="{{url('adminassets/js/hoverable-collapse.js')}}"></script>
  <script src="{{url('adminassets/js/template.js')}}"></script>
  <script src="{{url('adminassets/js/settings.js')}}"></script>
  <script src="{{url('adminassets/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
</body>


</html>
