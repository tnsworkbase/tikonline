<!DOCTYPE html>
<html lang="en">


<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Textile company - Mills and yarn manufactures</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="//cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css">

   <!-- <link rel="stylesheet" href="{{url('adminassets/vendors/font-awesome/css/font-awesome.min.css')}}"/>
    -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{url('adminassets/vendors/css/vendor.bundle.base.css')}}">
   
  <link rel="stylesheet" href="{{url('adminassets/css/horizontal-layout-light/style.css')}}">
  
  <!-- <link rel="shortcut icon" href="../../../../images/favicon.png" /> -->
</head>

<body>
  <div class="container-scroller">
    <div class="horizontal-menu">
      <nav class="navbar top-navbar col-lg-12 col-12 p-0">
        <div class="container">
          <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            <a class="navbar-brand brand-logo" href="index.html"><img src="{{url('adminassets/images/logo.png')}}" alt="logo"></a>
            <a class="navbar-brand brand-logo-mini" href="{{url('/')}}"><img src="{{url('adminassets/images/logo.png')}}" alt="logo"></a>
          </div>
          <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
            <ul class="navbar-nav mr-lg-4 w-100">
              <li class="nav-item nav-search d-none d-lg-block w-60">
                <div class="input-group">
                  
                  <input type="text" class="form-control" placeholder="SEARCH" aria-label="search" aria-describedby="search">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="search">
                      <i class="mdi mdi-magnify"></i>
                    </span>
                  </div>
                </div>
              </li>
            </ul>
            <ul class="navbar-nav navbar-nav-right">
              <li class="nav-item dropdown mr-1">
                <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-message-text mx-0"></i>
                  
                    &nbsp;Reviews
                </a>
          <!--       <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                  <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                        <img src="{{url('adminassets/images/faces/face4.jpg')}}" alt="image" class="profile-pic">
                    </div>
                    <div class="preview-item-content flex-grow">
                      <h6 class="preview-subject ellipsis font-weight-normal">David Grey
                      </h6>
                      <p class="font-weight-light small-text text-muted mb-0">
                        The meeting is cancelled
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                        <img src="../../images/faces/face2.jpg" alt="image" class="profile-pic">
                    </div>
                    <div class="preview-item-content flex-grow">
                      <h6 class="preview-subject ellipsis font-weight-normal">Tim Cook
                      </h6>
                      <p class="font-weight-light small-text text-muted mb-0">
                        New product launch
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                        <img src="../../images/faces/face3.jpg" alt="image" class="profile-pic">
                    </div>
                    <div class="preview-item-content flex-grow">
                      <h6 class="preview-subject ellipsis font-weight-normal"> Johnson
                      </h6>
                      <p class="font-weight-light small-text text-muted mb-0">
                        Upcoming board meeting
                      </p>
                    </div>
                  </a>
                </div> -->
              </li>
              <li class="nav-item dropdown mr-4">
                <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center notification-dropdown" id="notificationDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-bell mx-0"></i>
                  <span class="count"></span>

                </a>
                &nbsp;Updates
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                  <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-success">
                        <i class="mdi mdi-information mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-normal">Application Error</h6>
                      <p class="font-weight-light small-text mb-0 text-muted">
                        Just now
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-warning">
                        <i class="mdi mdi-settings mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-normal">Settings</h6>
                      <p class="font-weight-light small-text mb-0 text-muted">
                        Private message
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-info">
                        <i class="mdi mdi-account-box mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-normal">New user registration</h6>
                      <p class="font-weight-light small-text mb-0 text-muted">
                        2 days ago
                      </p>
                    </div>
                  </a>
                </div>
              </li>
              <!-- <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                  <img src="{{url('adminassets/images/faces/face5.jpg')}}" alt="profile">
                  <span class="nav-profile-name">Louis Barnett</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                  <a class="dropdown-item">
                    <i class="mdi mdi-settings text-secondary"></i>
                    Settings
                  </a>
                  <a class="dropdown-item">
                    <i class="mdi mdi-logout text-secondary"></i>
                    Logout
                  </a>
                </div>
              </li> -->
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
              <span class="mdi mdi-menu"></span>
            </button>
          </div>
        </div>
      </nav>
      <nav class="bottom-navbar">
        <div class="container">
          <ul class="nav page-navigation">

            <li class="nav-item active">
              <a class="nav-link" href="index.html">
              
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/widgets/widgets.html">
                <i class="mdi mdi-airplay menu-icon"></i>
                <span class="menu-title">My Works</span>
              </a>
            </li>

             <li class="nav-item" style="padding: 0 23em 0 0;">
              <a class="nav-link" href="pages/widgets/widgets.html">
                <i class="mdi mdi-airplay menu-icon"></i>
                <span class="menu-title">Confirmed Projects</span>
              </a>
            </li>
          
          
            <li class="nav-item">
                <a class="nav-link" id="messageDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-account-circle mx-0" style="font-size:20px;"></i>
                  &nbsp;Hello! &nbsp; <strong>User Name</strong>
                </a>
              </li>
             
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="mdi mdi-image-filter menu-icon"></i>
                  <span class="menu-title">MY Account</span>
                  <i class="menu-arrow"></i></a>
                <div class="submenu">
                  <ul class="submenu-item">
                    <li class="nav-item"><a class="nav-link" href="#">Email</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">User</a></li>
                    
                    <li class="nav-item"><a class="nav-link" href="#">Gallery</a></li>
                  </ul>
                </div>
              </li>

               
                <li class="nav-item">
              <a class="nav-link" href="{{url('/')}}">
                <i class="mdi mdi-airplay menu-icon"></i>
                <span class="menu-title">Logout</span>
              </a>
            </li>
          
          </ul>

           
        </div>
      </nav>
    </div>




  