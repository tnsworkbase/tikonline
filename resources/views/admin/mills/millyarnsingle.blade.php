@include('admin.common.header')


    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            
            <div class="col-lg-12 headtitle">
           <h3>Mills</h3>
           </div>
           <div class="col-lg-12 headtitle1">
            <h5>Yarn</h5>
          </div>
            <div class="col-lg-12  stretch-card mil1">
              <div class="card">

                <div class="card-body">
                   <h5 class="chooset1">Choose Single Yarn</h5>

                   
                      <form action="" method="">

                            <div class="form-group row offset-lg-3 choosesingle">
                              <label for="choose single" class="col-sm-2 col-form-label">Choose Single</label>
                            <div class="col-sm-4">
                              <select class="form-control" id="exampleFormControlSelect2">
                                  <option value="">100% cotton</option>
                                  <option value="">100% Viscos</option>
                                  <option value="">100% Model</option>
                                  <option value="">100% Polyster</option>
                              </select>
                            </div>
                            </div>
                          <hr>

                          <div class="form-group row">

                            <div class="col-sm-6 check11">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="single" id="membershipRadios1" value="spun">
                              Spun
                              <i class="input-helper"></i></label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="single" id="membershipRadios2" value="filament">
                              Filament
                              <i class="input-helper"></i></label>
                              </div>
                            </div>


                          </div>
                         
                          <div class="enterbtndiv enterbtndiv1">
                              <button type="button" class="btn btn-dark ">Back</button>
                                <input type="submit" name="submit" value="NEXT" class="btn btn-danger ">
                          </div>
                      </form>
                       
                  
                 
                </div>
              </div>
            </div>
          </div>
        </div>
        
       
    
      </div>
      
    </div>
    


@include('admin.common.footer')
