@include('admin.common.header')


    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            
            <div class="col-lg-12 headtitle">
           <h3>Mills</h3>
           </div>
           <div class="col-lg-12 headtitle1">
            <h5>Yarn</h5>
          </div>
            <div class="col-lg-12  stretch-card mil1">
              <div class="card">

                <div class="card-body">
                   <h5 class="chooset1">Choose Yarn Type</h5>

                   
                      <form action="" method="" >

                          
                    
                          <div class="form-group row  offset-lg-2 myarnf">

                            <div class="col-sm-2">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="myarnf" id="membershipRadios1" value="super_combed" >
                             Super Combed RL
                              <i class="input-helper"></i></label>
                              </div>
                            </div>
                            <div class="col-sm-2">
                              <div class="form-check check11">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="myarnf" id="membershipRadios2" value="combed_vl">
                              Combed VL
                              <i class="input-helper"></i></label>
                              </div>
                            </div>

                            <div class="col-sm-2">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="myarnf"  value="semicombed">
                              Semi Combed GL
                              <i class="input-helper"></i></label>
                              </div>
                            </div>

                            <div class="col-sm-2">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="myarnf"  value="dyedyarn">
                              Carded
                              <i class="input-helper"></i></label>
                              </div>
                            </div>


                            <div class="col-sm-2">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="myarnf" id="" value="oe">
                              OE
                              <i class="input-helper"></i></label>
                              </div>
                            </div>



                          </div>
                           <br>

                            <div class="form-group row col-lg-12 offset-lg-2 mill2">

                            <div class="col-sm-5 row">
                              <div class="form-check col-sm-3">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="type"  value="melange" >
                              Melange
                              <i class="input-helper"></i></label>

                              
                              </div>

                               <select class="form-control col-sm-6" name="melange_type">
                                  <option value="">Select Melange Type </option>
                                  <option value="">LT Grel Mel</option>
                                  <option value="">Ecru Mel</option>
                                  <option value="">Andhra Mel</option>
                                  <option value="">Charcoal Mel</option>
                                  <option value="">Oatmeal Mel</option>
                              </select>
                            </div>

                         

                          
                            <div class="col-sm-5 row">
                              <div class="form-check col-sm-3">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="type"  value="slub" >
                              Slub
                              <i class="input-helper"></i></label>

                              
                              </div>

                               <select class="form-control col-sm-5" name="slub_type">
                                  <option value="">Select Slub type</option>
                                  <option value="short">Short</option>
                                  <option value="medium">Medium</option>
                                  <option value="Long">Long</option>
                              </select>
                            </div>

                          </div>

                          <hr>
                   
                            <div class="form-group col-md-6 offset-md-3" style="text-align: center;">
                      <label for="exampleInputUsername1">Enter Your Denier</label>
                      <input type="text" class="form-control" id="exampleInputUsername1" placeholder="">
                    </div>

                          <div class="enterbtndiv enterbtndiv1">
                              <button type="button" class="btn btn-dark ">Back</button>
                                <input type="submit" name="submit" value="NEXT" class="btn btn-danger ">
                          </div>
                      </form>
                       
                  
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      
       
      
      </div>
    </div>
 


@include('admin.common.footer')
