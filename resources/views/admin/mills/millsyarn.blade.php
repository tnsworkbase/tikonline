@include('admin.common.header')


    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            
            <div class="col-lg-12 headtitle">
           <h3>Mills</h3>
           </div>
           <div class="col-lg-12 headtitle1">
            <h5>Yarn</h5>
          </div>
            <div class="col-lg-12  stretch-card mil1">
              <div class="card">

                <div class="card-body">
                   <h5 class="chooset1">Choose Yarn Type</h5>

                   
                      <form action="" method="">

                    
                    
                          <div class="form-group row offset-lg-3">

                            <div class="col-sm-3">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="yarntype" id="membershipRadios1" value="RegulaYarn" >
                              Regular Yarn
                              <i class="input-helper"></i></label>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-check check11">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="yarntype" id="membershipRadios2" value="specialyarn">
                              Special Yarn
                              <i class="input-helper"></i></label>
                              </div>
                            </div>

                            <div class="col-sm-3">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="yarntype" id="membershipRadios3" value="dyedyarn">
                              Dyed Yarn
                              <i class="input-helper"></i></label>
                              </div>
                            </div>


                          </div>
                          <hr>
                          <div class="enterbtndiv enterbtndiv1">
                              <button type="button" class="btn btn-dark ">Back</button>
                                <input type="submit" name="submit" value="NEXT" class="btn btn-danger ">
                          </div>
                      </form>
                       
                  
                 
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
       
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->


@include('admin.common.footer')
