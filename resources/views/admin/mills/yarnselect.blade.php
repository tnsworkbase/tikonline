@include('admin.common.header')


    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
            
            <div class="col-lg-12 headtitle">
           <h3>Mills</h3>
           </div>
           <div class="col-lg-12 headtitle1">
            <h5>Yarn</h5>
          </div>
            <div class="col-lg-12 stretch-card mil1">
              <div class="card">
                <div class="card-body">
                   <h5 class="chooset1">Choose Content</h5>
                      <form action="" method="">
                          <div class="form-group row">

                            <div class="col-sm-6 check11">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="content" id="membershipRadios1" value="yarn">
                              Yarn
                              <i class="input-helper"></i></label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="content" id="membershipRadios2" value="fabric">
                              Fabric
                              <i class="input-helper"></i></label>
                              </div>
                            </div>


                          </div>
                          <hr>
                          <div class="enterbtndiv enterbtndiv1">
                              <button type="button" class="btn btn-dark enterbtn">Back</button>
                                <button type="button" class="btn btn-danger enterbtn">Next</button>
                          </div>
                      </form>
                       
                  
                 
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
       
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->


@include('admin.common.footer')
