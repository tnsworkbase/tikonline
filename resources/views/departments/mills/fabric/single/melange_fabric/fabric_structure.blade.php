@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Fabric</div>
          <div class="inner-grid-box yarn-qty">
              <div class="input-cover-title">Choose required Structure</div>
              <div class="three-structure-types">
                <div class="three-yarn-cover">
                  <input type="radio" id="yarn" name="type" value="Yarn">
                  <label for="yarn">Jersey</label><br>
                  <select class="yarn-select" name="yarn" id="yarn">
                    <option value="1">Select Jersey Structure</option>
                    <option value="2">Single Jersey / Plain / Fine</option>
                    <option value="3">Airtex / Picque / Thick Picque</option>
                    <option value="4">Honey Comb</option>
                    <option value="5">Herringbone</option>
                    <option value="6">Bubble / Pattani</option>
                    <option value="7">Box Design</option>
                    <option value="8">Feeder Stripes</option>
                    <option value="9">2 Yarn Fleece / French Terry</option>
                    <option value="10">Mini Jacquard Designs</option>
                    <option value="11">Single Jersey + Lycra</option>
                    <option value="12">Picque + Lycra</option>
                    <option value="13">3 Colour Striper</option>
                    <option value="14">4 Colour Striper</option>
                    <option value="15">6 Colour Striper</option>
                    <option value="16">Striper Mini Jacquard</option>
                    <option value="17">Striper Full Jacquard</option>
                    <option value="18">Single Jersey Jacquard</option>
                    <option value="19">Single Jersey Mesh Jacquard</option>
                    <option value="20">Single Jersey Plating Eyelet Jacquard</option>
                  </select>
                </div>
                <div class="three-yarn-cover">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Interlock</label><br>
                  <select class="yarn-select" name="yarn" id="yarn">
                    <option value="1">Select Interlock Structure</option>
                    <option value="2">Plain Interlock</option>
                    <option value="3">4 track designs</option>
                    <option value="4">Interlock Striper</option>
                    <option value="5">Interlock Jacquard</option>
                    <option value="6">Mattress Ticking</option>
                    <option value="7">Plating Interlock</option>
                  </select>
                </div>
                <div class="three-yarn-cover">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Rib</label><br>
                  <select class="yarn-select" name="yarn" id="yarn">
                    <option value="1">Select Rib Structure</option>
                    <option value="2">Rib</option>
                    <option value="3">1 X 1 Rib</option>
                    <option value="4">2 X 2 Rib</option>
                    <option value="5">Rib + Lyrca</option>
                    <option value="6">Flat Back Rib</option>
                    <option value="7">Rib Transfer (pattern wheel)</option>
                    <option value="2">Rib Striper</option>
                    <option value="2">Rib Transfer Jacquard</option>
                  </select>
                </div>
              </div>
              <p class="para">If Lycra required along with your chosen structure enter the follow details sa per your requirements</p>
                <div class="three-structure-types">
                  <div class="three-yarn-cover">
                    <label for="yarn">Choose your Lycra percentage</label><br>
                    <select class="yarn-select" name="yarn" id="yarn">
                      <option value="1">Select %</option>
                      <option value="2">Single Jersey / Plain / Fine</option>
                      <option value="3">Airtex / Picque / Thick Picque</option>
                      <option value="4">Honey Comb</option>
                      <option value="5">Herringbone</option>
                    </select>
                  </div>
                  <div class="three-yarn-cover">
                    <label for="fabric">Choose your Lycra Denier</label><br>
                    <select class="yarn-select" name="yarn" id="yarn">
                      <option value="1">Select Denier</option>
                      <option value="2">Plain Interlock</option>
                      <option value="3">4 track designs</option>
                    </select>
                  </div>
                  <div class="three-yarn-cover feeder">
                    <label for="fabric">Choose Lycra Feeder</label><br>
                    <input type="radio" id="fabric" name="type" value="Fabric">
                    <label for="fabric">All Feeder</label>
                    <input type="radio" id="fabric" name="type" value="Fabric">
                    <label for="fabric">Alternate Feeder</label>
                  </div>
                </div>
            <div class="input-btn">
              <input type="sumit" name="sumit" class="btn-black" value="back">
              <input type="sumit" name="sumit" class="btn-red" value="enter">
            </div>
          </div>
        </form>
    </div>
  </div>
</section>
@endsection

@section('front_script')

@endsection

@endsection

@section('front_script')

@endsection
