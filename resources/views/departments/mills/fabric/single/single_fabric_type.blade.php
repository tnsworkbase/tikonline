@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Fabric</div>
          <div class="inner-grid-box yarn">
              <div class="input-cover-title">Choose the Single</div>
                  <label for="yarn">Single</label>
                  <select class="yarn-select" name="yarn" id="yarn">
                    <option value="1">Select Single type</option>
                    <option value="2">Select Single type</option>
                    <option value="3">Select Single type</option>
                    <option value="4">Select Single type</option>
                  </select>
            </div>

          <div class="inner-second-grid">
              <div class="input-cover yarn">
                <div class="input-left">
                  <input type="radio" id="yarn" name="type" value="Yarn">
                  <label for="yarn">Spun</label><br>
                </div>
                <div class="input-left">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Filament</label><br>
                </div>
              </div>
              <div class="input-btn">
                <input type="sumit" name="sumit" class="btn-black" value="back">
                <input type="sumit" name="sumit" class="btn-red" value="next">
              </div>
          </div>
        </form>
  </div>
</section>
@endsection

@section('front_script')

@endsection
