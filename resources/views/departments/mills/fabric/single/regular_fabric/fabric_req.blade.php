@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Fabric</div>
          <div class="inner-grid-box yarn-qty">
              <div class="input-cover-title">Enter below details as per your requirement</div>

              <div class="gauge-details">
                <div class="gauge-detail-cover">
                  <div class="gauge-title">Gauge</div>
                  <div class="gauge-subtitle">Select one</div>
                  <select class="yarn-select" name="yarn" id="yarn">
                    <option value="1">Select Gauge Value</option>
                    <option value="2">Select Gauge Value</option>
                  </select>
                </div>
                <div class="gauge-detail-cover">
                  <div class="gauge-title">DIA</div>
                  <div class="gauge-subtitle">Select one or multiple</div>
                  <select class="yarn-select" name="yarn" id="yarn">
                    <option value="1">23</option>
                    <option value="2">25</option>
                    <option value="2">28</option>
                    <option value="2">Enter DIA</option>
                  </select>
                </div>
                <div class="gauge-detail-cover">
                  <div class="gauge-subtitle">Select one</div>
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">OPW</label>
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">TUB</label>
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">TUB with Needle Drop</label>
                </div>
              </div>

            <div class="input-btn">
              <input type="sumit" name="sumit" class="btn-black" value="back">
              <input type="sumit" name="sumit" class="btn-red" value="enter">
            </div>
          </div>
        </form>
    </div>
  </div>
</section>
@endsection

@section('front_script')

@endsection
