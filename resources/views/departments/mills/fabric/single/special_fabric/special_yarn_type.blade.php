@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Fibre</div>
          <div class="inner-grid-box yarn">
              <div class="input-cover-title">Choose Special Yarn type</div>
                  <label for="yarn">Special Yarn type</label>
                  <select class="yarn-select" name="yarn" id="yarn">
                    <option value="1">100% Cotton</option>
                    <option value="2">100% Viscose</option>
                    <option value="3">100% Modal</option>
                    <option value="4">100% Polyster</option>
                  </select>
            </div>

          <div class="inner-second-grid">
              <div class="input-btn">
                <input type="sumit" name="sumit" class="btn-black" value="back">
                <input type="sumit" name="sumit" class="btn-red" value="next">
              </div>
          </div>
        </form>
  </div>
</section>
@endsection

@section('front_script')

@endsection
