@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Fabric</div>
          <div class="inner-grid-box">
              <div class="input-cover-title">Choose Yarn Content</div>
              <div class="input-cover">
                <div class="input-left">
                  <input type="radio" id="yarn" name="type" value="Yarn">
                  <label for="yarn">Single</label><br>
                </div>
                <div class="input-left">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Blends</label><br>
                </div>
              </div>
            <div class="input-btn">
              <input type="sumit" name="sumit" class="btn-black" value="back">
              <input type="sumit" name="sumit" class="btn-red" value="enter">
            </div>
          </div>
        </form>
    </div>
  </div>
</section>

@endsection

@section('front_script')

@endsection
