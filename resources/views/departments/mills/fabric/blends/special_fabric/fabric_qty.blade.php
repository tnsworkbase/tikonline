@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Fabric</div>
          <div class="inner-grid-box yarn-qty">
              <div class="input-cover-title">Choose Yarn Quality</div>
              <div class="four-yarn-types">
                <div class="four-yarn-cover">
                  <input type="radio" id="yarn" name="type" value="Yarn">
                  <label for="yarn">Super Combed RL</label><br>
                </div>
                <div class="four-yarn-cover">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Combed VL</label><br>
                </div>
                <div class="four-yarn-cover">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Semi Combed GL</label><br>
                </div>
              </div>
              <div class="field-denier">
                <form class="denier-form" action="/action_page.php">
                    <label for="fname">Enter your Count</label><br>
                    <input type="text" id="fname" name="fname" value="">
                  </form>
              </div>
            <div class="input-btn">
              <input type="sumit" name="sumit" class="btn-black" value="back">
              <input type="sumit" name="sumit" class="btn-red" value="next">
            </div>
          </div>
        </form>
    </div>
  </div>
</section>
@endsection

@section('front_script')

@endsection
