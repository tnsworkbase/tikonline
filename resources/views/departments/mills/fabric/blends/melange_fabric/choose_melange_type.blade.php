@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Fabric</div>
          <div class="inner-grid-box yarn">
              <div class="input-cover-title">Choose the type of Melange or Slub</div>
              <div class="melang-grid">
                <div class="melang-grid-cover">
                    <label for="yarn">Melange</label>
                    <select class="yarn-select" name="yarn" id="yarn">
                      <option value="1">LT Grey Mel</option>
                      <option value="2">Combed VL</option>
                      <option value="3">Semi Combed GL</option>
                      <option value="4">Carded</option>
                      <option value="5">OE</option>
                    </select>
                </div>
                <div class="melang-grid-cover">
                    <label for="yarn">Slub</label>
                    <select class="yarn-select" name="yarn" id="yarn">
                      <option value="1">Short Slub</option>
                      <option value="2">Combed VL</option>
                      <option value="3">Semi Combed GL</option>
                      <option value="4">Carded</option>
                      <option value="5">OE</option>
                    </select>
                </div>
              </div>
            </div>

          <div class="inner-second-grid">
            <div class="field-denier">
              <form class="denier-form" action="/action_page.php">
                  <label for="fname">Enter your Count</label><br>
                  <input type="text" id="fname" name="fname" value="">
                </form>
            </div>
              <div class="input-btn">
                <input type="sumit" name="sumit" class="btn-black" value="back">
                <input type="sumit" name="sumit" class="btn-red" value="enter">
              </div>
          </div>
        </form>
  </div>
</section>
@endsection

@section('front_script')

@endsection
