@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Fabric</div>
          <div class="inner-grid-box yarn-qty">
              <div class="input-cover-title">Choose the Blends</div>
              <div class="blend-selection-grid">
                <label for="yarn">Blends</label>
                <select class="yarn-select" name="yarn" id="yarn">
                  <option value="1">Select Blends</option>
                  <option value="2">Select Blends</option>
                  <option value="3">Select Blends</option>
                  <option value="4">Select Blends</option>
                </select>
              </div>
              <div class="blend-combination">
                <div class="input-cover-title comb">Enter your required Blend Combination</div>
                <div class="blend-types">
                  <div class="blend-types-cover left">
                    <div class="gauge-title">Cotton</div>
                    <input type="text" id="fname" name="fname" placeholder="Enter">
                  </div>
                  <div class="blend-types-cover">
                    <div class="gauge-title">Poly</div>
                    <input type="text" id="fname" name="fname" placeholder="Enter">
                  </div>
                </div>
              </div>
            <div class="input-btn">
              <input type="sumit" name="sumit" class="btn-black" value="back">
              <input type="sumit" name="sumit" class="btn-red" value="next">
            </div>
          </div>
        </form>
    </div>
  </div>
</section>
@endsection

@section('front_script')

@endsection
