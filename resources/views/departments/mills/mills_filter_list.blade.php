@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title">mills<h2>
        <div class="input-cover-title">Choose required filter</div>
    </div>
    <div class="enquiry-select-block">
        <label for="yarn">Send Enquiry by:</label>
          <div class="enquiry-select-cover">
            <select class="select-black-btn" name="yarn" id="yarn">
              <option value="1">Selected Units</option>
            </select>
        </div>
        <div class="enquiry-select-cover">
          <select class="select-black-btn" name="yarn" id="yarn">
            <option value="1">For All</option>
          </select>
        </div>
        <div class="enquiry-select-cover">
          <select class="select-black-btn" name="yarn" id="yarn">
            <option value="1">Rating Above</option>
          </select>
        </div>
        <div class="enquiry-select-cover">
          <select class="select-black-btn" name="yarn" id="yarn">
            <option value="1">Certifications</option>
            <option value="2">Certifications One</option>
            <option value="3">Certifications Two</option>
            <option value="4">Certifications Three</option>
            <option value="4">Certifications Four</option>
            <option value="4">Certifications Five</option>
          </select>
      </div>
  </div>
  <div class="selection-table-block">
    <div class="table-title">Yarn</div>
    <table class="table req-filter">
      <thead class="thread-head">
        <tr>
          <th scope="col">Select</th>
          <th scope="col">S No</th>
          <th scope="col">Mill Name</th>
          <th scope="col">Content</th>
          <th scope="col">Type</th>
          <th scope="col">Count</th>
          <th scope="col">Credit Period</th>
          <th scope="col">Reviews</th>
        </tr>
      </thead>
      <tbody>
        <tr class="select-table-row">
          <th scope="row"><input type="checkbox" id="yarn" name="type" value="Yarn"></th>
          <td>001</td>
          <td>KPR and Sons Yarn & Fabrication</td>
          <td>100% Cotton</td>
          <td>Combed</td>
          <td>30</td>
          <td>10-15 Days</td>
          <td><i class="far fa-star"></i></td>
        </tr>
        <tr>
          <th scope="row"><input type="checkbox" id="yarn" name="type" value="Yarn"></th>
          <td>002</td>
          <td>YBM Yarn & Fabrication</td>
          <td>100% Cotton</td>
          <td>Combed</td>
          <td>30</td>
          <td>10-15 Days</td>
          <td><i class="far fa-star"></i></td>
        </tr>
        <tr>
          <th scope="row"><input type="checkbox" id="yarn" name="type" value="Yarn"></th>
          <td>003</td>
          <td>KPR and Sons Yarn & Fabrication</td>
          <td>100% Cotton</td>
          <td>Combed</td>
          <td>30</td>
          <td>10-15 Days</td>
          <td><i class="far fa-star"></i></td>
        </tr>
        <tr>
          <th scope="row"><input type="checkbox" id="yarn" name="type" value="Yarn"></th>
          <td>004</td>
          <td>YBM Yarn & Fabrication</td>
          <td>100% Cotton</td>
          <td>Combed</td>
          <td>30</td>
          <td>10-15 Days</td>
          <td><i class="far fa-star"></i></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="input-btn">
    <input type="sumit" name="sumit" class="btn-black" value="back">
    <a href="{{URL::to('mills/mills_company_req')}}">
      <input type="sumit" name="sumit" class="btn-red" value="next">
    </a>
  </div>

  </div>
</section>

@endsection

@section('front_script')

@endsection
