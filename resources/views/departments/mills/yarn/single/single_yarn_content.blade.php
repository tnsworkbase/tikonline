@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Yarn</div>
          <div class="inner-grid-box">
              <div class="input-cover-title">Choose Yarn Type</div>
              <div class="four-yarn-types">
                <div class="four-yarn-cover">
                  <input type="radio" id="yarn" name="type" value="Yarn" checked>
                  <label for="yarn">Regular Yarn</label><br>
                </div>
                <div class="four-yarn-cover">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Special Yarn</label><br>
                </div>
                <div class="four-yarn-cover">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Dyed Yarn</label><br>
                </div>
                <div class="four-yarn-cover">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Melange / Slub Yarn</label><br>
                </div>
              </div>
            <div class="input-btn">
              <input type="sumit" name="sumit" class="btn-black" value="back">
              <a href="{{URL::to('mills/y-s-r-yarn_qty')}}">
                <input type="sumit" name="sumit" class="btn-red" value="next">
              </a>
            </div>
          </div>
        </form>
    </div>
  </div>
</section>

@endsection

@section('front_script')

@endsection
