@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
      <h2 class="section-title">mills<h2>
        <form class="inner-selection-form yarn" action="/action_page.php">
          <div class="inner-sub-title">Yarn</div>
          <div class="inner-grid-box yarn">
              <div class="input-cover-title">Choose Single Yarn Content</div>
                  <label for="yarn">Choose single</label>
                  <select class="yarn-select" name="yarn" id="yarn">
                    <option value="1">100% Cotton</option>
                    <option value="2">100% Viscose</option>
                    <option value="3">100% Modal</option>
                    <option value="4">100% Polyster</option>
                  </select>
            </div>

          <div class="inner-second-grid">
              <div class="input-cover yarn">
                <div class="input-left">
                  <input type="radio" id="yarn" name="type" value="Yarn" checked>
                  <label for="yarn">Spun</label><br>
                </div>
                <div class="input-left">
                  <input type="radio" id="fabric" name="type" value="Fabric">
                  <label for="fabric">Filament</label><br>
                </div>
              </div>
              <div class="input-btn">
                <input type="sumit" name="sumit" class="btn-black" value="back">
                <a href="{{URL::to('mills/y-s-single_yarn_content')}}">
                  <input type="sumit" name="sumit" class="btn-red" value="next">
                </a>
              </div>
          </div>
        </form>
  </div>
</section>

@endsection

@section('front_script')

@endsection
