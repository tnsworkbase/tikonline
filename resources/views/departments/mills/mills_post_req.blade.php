@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title period">mills<h2>
        <div class="table-title">Yarn</div>
        <div class="requirement-grids">
          <div class="requirement-para red">Summary of your Enquiry</div>
          <div class="requirement-grid-overall">
              <div class="requirement-grid-box left">
                <div class="requirement-grid-cover summary">
                  <label for="fname">Grey Yarn</label><br>
                  <input type="text" id="fname" name="fname" value="Single">
                </div>
                <div class="requirement-grid-cover summary">
                  <label for="fname">Content</label><br>
                  <input type="text" id="fname" name="fname" value="100% Viscose Spun">
                </div>
                <div class="requirement-grid-cover summary">
                  <label for="fname">Type</label><br>
                  <input type="text" id="fname" name="fname" value="Combed">
                </div>
                <div class="requirement-grid-cover summary">
                  <label for="fname">Count</label><br>
                  <input type="text" id="fname" name="fname" value="50">
                </div>
                <div class="requirement-grid-cover summary">
                  <label for="fname">No of Kgs</label><br>
                  <input type="text" id="fname" name="fname" value="45">
                </div>
                <div class="requirement-grid-cover summary price">
                  <label for="fname">Your required Price</label>
                  <div class="price-form">
                    <input type="checkbox" id="yarn" name="type" value="Yarn">
                    <label for="yarn">Price requested from Supplier</label>
                  </div>
                </div>
              </div>
              <div class="requirement-grid-box right">
                <div class="requirement-grid-cover summary">
                  <label for="fname">Your Delivery Period</label><br>
                  <input type="text" id="fname" name="fname" value="50 Days">
                </div>
                <div class="requirement-grid-cover summary">
                  <label for="fname">Your Credit Period</label><br>
                  <input type="text" id="fname" name="fname" value="35 Days">
                </div>
              </div>
              <div class="input-btn">
                <input type="sumit" name="sumit" class="btn-black" value="back">
                <a href="{{URL::to('mills/y-s-single_yarn_content')}}">
                  <input type="sumit" name="sumit" class="btn-red" value="Submit Enquiry">
                </a>
              </div>
            </div>
          </div>
    </div>
  </div>
</section>

@endsection

@section('front_script')

@endsection
