@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

   <section class="mills-block-section">
     <div class="container">
       <div class="section-content">
         <h2 class="section-title">mills<h2>
           <form class="inner-selection-form" action="/action_page.php">
             <div class="input-cover">
               <div class="input-left">
                 <input type="radio" id="yarn" name="type" value="Yarn" checked>
                 <label for="yarn">Yarn</label><br>
               </div>
               <div class="input-left">
                 <input type="radio" id="fabric" name="type" value="Fabric">
                 <label for="fabric">Fabric</label><br>
               </div>
             </div>
             <div class="input-btn">
               <a href="{{URL::to('mills/y-yarn_type')}}">
                 <input type="sumit" name="sumit" class="btn-red" value="enter">
               </a>
             </div>
           </form>
       </div>
     </div>
   </section>

@endsection

@section('front_script')

@endsection
