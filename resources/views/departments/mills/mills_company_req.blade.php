@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

<section class="mills-block-section">
  <div class="container">
    <div class="section-content">
      <h2 class="section-title period">mills<h2>
        <div class="table-title">Yarn</div>
        <div class="requirement-grids">
          <div class="requirement-para">Enter below details as per your requirements</div>
          <div class="requirement-grid-box">
            <div class="requirement-grid-cover">
              <label for="fname">Enter No of Kgs</label>
              <input type="text" id="fname" name="fname" value="" placeholder="45">
            </div>
            <div class="requirement-grid-cover">
              <label for="fname">Price</label><br>
              <div class="requirement-grid-inner">
                <label for="fname">Enter your required Price</label>
                <input type="text" id="fname" name="fname" value=""><br>
                <span>OR</span><br>
                <div class="price-request">
                  <a href="#">
                    <span>Request Price</span>
                    <i class="fas fa-caret-right" style="position: absolute; top: 11px; right: 11px;"></i>
                  </a>
                </div>
              </div>


            </div>
            <div class="requirement-grid-cover">
              <label for="fname">Enter your required Credit Period</label>
              <input type="text" id="fname" name="fname" value="" placeholder="35 Days">
            </div>
            <div class="requirement-grid-cover">
              <label for="fname">Enter your required Delivery Period</label>
              <input type="text" id="fname" name="fname" value="" placeholder="50 Days">
            </div>
          </div>

          <div class="input-btn">
            <input type="sumit" name="sumit" class="btn-black" value="back">
            <a href="{{URL::to('mills/mills_post_req')}}">
              <input type="sumit" name="sumit" class="btn-red" value="next">
            </a>
          </div>
        </div>

    </div>
  </div>
</section>

@endsection

@section('front_script')

@endsection
