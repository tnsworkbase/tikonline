  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <footer class="footer-area footer--light">
      <div class="footer-big">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-12">
              <div class="footer-widget">
                <div class="widget-about">
                  <img src="{{url('themeassets/img/logo.png')}}" alt="" class="img-fluid">
                  <div class="inner-addon1 left-addon1">
                    <i class="fa fa-arrow-right"></i>
                    <input type="text" class="form-control" placeholder="Enter your Email" />
                  </div>

                  <ul class="contact-details">
                      <h4 class="footer-widget-title" style="margin-bottom: 3px;">Location</h4>
                   
                      <p>1/22,Raja street Tiruppur Tamilnadu</p>
                      <a href="tel:344-755-111" style="color: #fff">+91999999999</a>
                      <br>
                      <a href="tel:344-755-111" style="color: #fff;">+91999999999</a>
                      <br>
                      <span class="icon-envelope-open"></span>
                      <a href="mailto:support@textile.com">support@textile.com</a>
                    
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-4">
              <div class="footer-widget">
                <div class="footer-menu footer-menu--1">
                  <h4 class="footer-widget-title">Popular Category</h4>
                  <ul>
                    <li>
                      <a href="#">Yarns</a>
                    </li>
                    <li>
                      <a href="#">Mens wear</a>
                    </li>
                    <li>
                      <a href="#">Womens Wear</a>
                    </li>
                    <li>
                      <a href="#">T-Shirts</a>
                    </li>
                    <li>
                      <a href="#">Cotton cloths</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
        

            <div class="col-md-3 col-sm-4">
              <div class="footer-widget">
                <div class="footer-menu">
                  <h4 class="footer-widget-title">Quick Links </h4>
                  <ul>
                    <li>
                      <a href="#">About Us</a>
                    </li>
                    <li>
                      <a href="#">Category</a>
                    </li>
                    <li>
                      <a href="#">Affiliates</a>
                    </li>
                    <li>
                      <a href="#">Register</a>
                    </li>
                    <li>
                      <a href="#">Contact Us</a>
                    </li>
                    
                  </ul>
                </div>
              </div>
            </div>
           
        
            
             <div class="col-md-3 col-sm-4">
              <div class="footer-widget">
                <div class="footer-menu">
                  <h4 class="footer-widget-title">Our Company</h4>
                  <ul>
                    <li>
                      <a href="#">About Us</a>
                    </li>
                    <li>
                      <a href="#">How It Works</a>
                    </li>
                    <li>
                      <a href="#">Affiliates</a>
                    </li>
                    <li>
                      <a href="#">Testimonials</a>
                    </li>
                    <li>
                      <a href="#">Contact Us</a>
                    </li> 
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-12">
                
                <div class="col-md-offset-3 col-md-9 col-sm-6 hrfooter">
                     <hr>
                    

            <a href="#" target="_blank" class="btn-social btn-facebook"><i class="fab fa-facebook-f"></i></a>
             <a href="#" target="_blank" class="btn-social btn-twitter"><i class="fab fa-twitter"></i></a>
              <a href="#" target="_blank" class="btn-social btn-linkedin"><i class="fab fa-linkedin"></i></a>
            <a href="#" target="_blank" class="btn-social btn-youtube"><i class="fab fa-youtube"></i></a>

            © 2020. Textile.All Right Reserved.

            </div>
             </div>
          </div>
        </div>
      </div>
  </footer>

    <div id="scroll-top">
        <span></span><i class="ion-chevron-right"></i><i class="ion-chevron-right"></i>
    </div>
 
    <script src="{{url('themeassets/js/vendors.js')}}"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    
    <script src="{{url('themeassets/js/active.js')}}"></script>
<script type="text/javascript">
  $(function() {
  $('.grid-item2').hover(function() {
    $('#cats').css('background-color', 'red');
  }, function() {
    // on mouseout, reset the background colour
    $('#cats').css('background-color', '');
  });
});
</script>
</body>
</html>