    <div class="hero-slider-area section-space">
        <!--=======  hero slider wrapper  =======-->

        <div class="hero-slider-wrapper">
            <div class="ht-slick-slider" data-slick-setting='{
            "slidesToShow": 1,
            "slidesToScroll": 1,
            "arrows": true,
            "dots": true,
            "autoplay": true,
            "autoplaySpeed": 5000,
            "speed": 1000,
            "fade": true,
            "infinite": true,
            "prevArrow": {"buttonClass": "slick-prev", "iconClass": "ion-chevron-left" },
            "nextArrow": {"buttonClass": "slick-next", "iconClass": "ion-chevron-right" }
        }' data-slick-responsive='[
            {"breakpoint":1501, "settings": {"slidesToShow": 1} },
            {"breakpoint":1199, "settings": {"slidesToShow": 1, "arrows": false} },
            {"breakpoint":991, "settings": {"slidesToShow": 1, "arrows": false} },
            {"breakpoint":767, "settings": {"slidesToShow": 1, "arrows": false} },
            {"breakpoint":575, "settings": {"slidesToShow": 1, "arrows": false} },
            {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
        ]'>

                <!--=======  single slider item  =======-->

                <div class="single-slider-item">
                    <div class="hero-slider-item-wrapper hero-slider-item-wrapper--fullwidth hero-slider-bg-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="hero-slider-content hero-slider-content--left-space">
                                       
                                        <p class="slider-title slider-title--big-light">All your textile Needs</p>
                                        <p class="slider-title slider-title--small">Check out the major milestones and achievements in the history of  Textiles that have led to it becoming one of the largest spinning mill in india</p>
                                        <a class="hero-slider-button" href="#"> <i class="fa fa-mius" style="width: 60px;height: 1.5px; background-color: #9b3a68;margin-bottom: 3px;margin-right: 15px;"></i>
                                            
                                        </span> Know More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--=======  End of single slider item  =======-->

                <!--=======  single slider item  =======-->

                <div class="single-slider-item">
                    <div class="hero-slider-item-wrapper hero-slider-item-wrapper--fullwidth hero-slider-bg-2">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="hero-slider-content hero-slider-content--left-space">
                                        <p class="slider-title slider-title--big-light">AMAZING PRODUCT!</p>
                                       <!--  <p class="slider-title slider-title--big-light">DECOR CHAIR</p> -->
                                        <p class="slider-title slider-title--small">A range of textiles for homes including lining bed linen,bathing items and upholstery like flat and good collections for all customers </p>
                                        <a class="hero-slider-button" href="#"><i class="fa fa-mius" style="width: 60px;height: 1.5px; background-color: #9b3a68;margin-bottom: 3px;margin-right: 15px;"></i> Know More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                

                <div class="single-slider-item">
                    <div class="hero-slider-item-wrapper hero-slider-item-wrapper--fullwidth hero-slider-bg-3">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="hero-slider-content hero-slider-content--left-space">
                                        <p class="slider-title slider-title--big-light">New Arrivals</p>
                                        <!-- <p class="slider-title slider-title--big-light">Decorative Vases</p> -->
                                        <p class="slider-title slider-title--small"> one of the largest manufacturers of yarn, fabrics, garments, home textiles and organic products in India and with exports to almost every continent</p>
                                        <a class="theme-button hero-slider-button" href="#"> <i class="fa fa-mius" style="width: 60px;height: 1.5px; background-color: #9b3a68;margin-bottom: 3px;margin-right: 15px;"></i> Know More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

             

            </div>
        </div>

       
    </div>
