<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/img/favicon.ico">

    <link href="{{url('themeassets/css/vendors.css')}}" rel="stylesheet">
    <link href="{{url('themeassets/css/style.css')}}" rel="stylesheet">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">



</head>

<body>

    <div class="header-area header-area--transparent header-area--middle-logo header-sticky">
        <div class="container wide">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header-wrapper d-none d-lg-flex">
                            <ul class="icon-list">
                                <li>
                                    <li>
                                        <div class="header-settings-icon">
                                            <a href="javascript:void(0)" class="header-settings-trigger" id="header-settings-trigger">
                                                <div class="setting-button">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                        </ul>
                        <div class="logo text-center">
                            <a href="{{url('/')}}">
                                <img src="{{url('themeassets/img/logo.png')}}" class="img-fluid" alt="">
                            </a>
                        </div>

                    
                        <div class="header-icon-wrapper" style="flex-basis: 80%;">
                            <ul class="icon-list">
                                
                                    <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="SEARCH CATEGORY" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class=" fa fa-search"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                                </li>
                               <!--  <li>
                                    <a href="javascript:void(0)" id="search-overlay-trigger">
                                        <i class="ion-ios-search-strong"></i>
                                    </a>
                                </li> -->
                                <li class="lgli loginmenu">
                                    <div class="header-cart-icon">
                                        <a href="{{url('/login')}}" id="">
                                            <i class="fa fa-user-circle"></i> LOGIN 
                                         
                                        </a> <i style="color: #fff">/</i>
                                         <a href="{{url('/register')}}" id="minicarts-trigger">
                                          &nbsp;REGISTER 
                                         
                                        </a>
                                        <!-- mini cart  -->
                                       
                                    </div>
                                </li>
                             
                            </ul>
                        </div>
                    </div>

                    <div class="header-mobile-navigation d-block d-lg-none">
                        <div class="row align-items-center">
                            <div class="col-6 col-md-6">
                                <div class="header-logo">
                                    <a href="{{url('/')}}">
                                        <img src="{{url('themeassets/img/logo.png')}}" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-6 col-md-6">
                                <div class="mobile-navigation text-right">
                                    <div class="header-icon-wrapper">
                                        <ul class="icon-list justify-content-end">
                                          
                                            <li>
                                                <a href="javascript:void(0)" class="mobile-menu-icon" id="mobile-menu-trigger"><i class="fa fa-bars"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

        

                </div>
            </div>
        </div>
    </div>




     <div class="offcanvas-mobile-menu" id="offcanvas-mobile-menu">
        <a href="javascript:void(0)" class="offcanvas-menu-close" id="offcanvas-menu-close-trigger">
            <i class="ion-android-close"></i>
        </a>

        <div class="offcanvas-wrapper">

            <div class="offcanvas-inner-content">
                <div class="offcanvas-mobile-search-area">
                    <form action="#">
                        <input type="search" placeholder="Search ...">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <nav class="offcanvas-navigation">
                    <ul>
                        <li class="menu-item-has-children"><a href="{{url('/')}}">Home</a>
                            
                        </li>
                         <li class="menu-item-has-children"><a href="{{url('/')}}">About us</a>
                            
                        </li>
                        <li class="menu-item-has-children"><a href="#">Category</a>
                            <ul class="sub-menu">
                                <li class="menu-item-has-children"><a href="#">Wearsne</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Knitting</a></li>
                                        <li><a href="#">Dying</a></li>
                                        <li><a href="#">Mills</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="#">Mens</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Jeans</a></li>
                                        <li><a href="#">T-shirts</a></li>
                                        <li><a href="#">Pants</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="#">Wears</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Shirts</a></li>
                                        <li><a href="#">Pants</a></li>
                                       
                                    </ul>
                                </li>

                            </ul>
                        </li>
                       <li class="menu-item-has-children"><a href="#">My Account</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Login</a></li>
                                        <li><a href="#">Register</a></li>
                                       
                                    </ul>
                                </li>
                     

                    </ul>
                </nav>

              

                <div class="offcanvas-widget-area">
                    <div class="off-canvas-contact-widget">
                        <div class="header-contact-info">
                            <ul class="header-contact-info__list">
                                <li><i class="ion-android-phone-portrait"></i> <a href="tel://12452456012">999 999 9999 </a></li>
                                <li><i class="ion-android-mail"></i> <a href="mailto:info@textile.com">info@textile.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--Off Canvas Widget Social Start-->
                    <div class="off-canvas-widget-social">
                        <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                        <a href="#" title="Youtube"><i class="fa fa-youtube-play"></i></a>
                        <a href="#" title="Vimeo"><i class="fa fa-vimeo-square"></i></a>
                    </div>
                    <!--Off Canvas Widget Social End-->
                </div>
            </div>
        </div>

    </div>



    <div class="search-overlay" id="search-overlay">
        <a href="javascript:void(0)" class="close-search-overlay" id="close-search-overlay">
            <i class="ion-ios-close-empty"></i>
        </a>

       

        <div class="search-form">
            <form action="#">
                <input type="search" placeholder="Search entire store here ...">
                <button type="submit"><i class="ion-android-search"></i></button>
            </form>
        </div>

        
    </div>
