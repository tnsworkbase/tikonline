@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

    <!--====================  page content area ====================-->
    <div class="page-content-area loginpage" >
        <div class="">
            <div class="row">
                <div class="col-lg-12">

                    <!--=======  page wrapper  =======-->
                    <div class="page-wrapper">
                        <div class="page-content-wrapper" style="padding-bottom: 0px;border-bottom: 0px;">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-5 bglogin" >
                                    <!-- Login Form s-->
                                    <div style="background-image: url('{{ asset('themeassets/img/backgrounds/login.jpg')}}');">
                                    	
                                    	<h1>Welcome to Textile</h1>
                                    	<h3>Login to continue Access</h3>
                                    	<p>Today we are thinking of all our members across the world and are encouraged by the support our networks bring in times such as these. Use the left test instrument details to attempt a transaction on the staging environment and  login.</p>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12 bgform">
                                    
                                    <form action="#">

                                        <div class="login-form">
                                            <h4 class="login-title colorred">Welcome back, Log in into account</h4>
                                           <hr>
                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <!-- <label>Email Address*</label> -->
                                                    <input type="email" class="fas" 
                                                    placeholder="&#xf0e0 &nbsp; Email Address">
                                                </div>
                                                <div class="col-12">
                                                   <!--  <label>Password</label> -->
                                                    <input type="password" class="fas" placeholder=" &#xf023 &nbsp; Password">
                                                </div>
                                                <div class="col-sm-6">

                                                    <div class="check-box d-inline-block ml-0 ml-md-2">
                                                        <input type="checkbox" id="remember_me" >
                                                        <label for="remember_me" style="color:#05a6dc;font-size: 12px;font-weight: bolder;">Remember me</label>
                                                    </div>

                                                </div>

                                                <div class="col-sm-6 text-left text-sm-right">
                                                    <a href="#" class="forget-pass-link"> Forgot passward ?</a>
                                                </div>
                                                 
                                                 

                                                 <div class="col-sm-6">

                                                    <div class="check-box d-inline-block ml-0 ml-md-2">
                                                        <button class="register-button btn-lg">SIGN IN</button>
                                                    </div>

                                                </div>

                                                <div class="col-sm-6 text-left text-sm-right acc">
                                                    <i>Don't have an account yet?</i>
                                                    <a href="{{url('/register')}}" class="s"> Create an account ?</a>
                                                </div>

                                                  
                                                
                                                    <!-- <button class="register-button">Login</button> -->
                                                
                                                  <hr style="width: 100%;margin-top: 5%;">

                                                  <p class="s1">By Signing  up you agree to Textile's <i> Terms and Conditions & privacy Policy</i></p>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--=======  End of page wrapper  =======-->
                </div>
            </div>
        </div>
</div>
@endsection

@section('front_script')

@endsection