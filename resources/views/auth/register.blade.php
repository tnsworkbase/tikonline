@extends('layouts.front_common')

@section('front_style')

@endsection
@section('content')

    <!--====================  page content area ====================-->
    <div class="page-content-area loginpage" >
        <div class="">
            <div class="row">
                <div class="col-lg-12">

                    <!--=======  page wrapper  =======-->
                    <div class="page-wrapper">
                        <div class="page-content-wrapper" style="padding-bottom: 0px;border-bottom: 0px;">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-5 bglogin bgreg" >
                                    <!-- Login Form s-->
                                    <div style="background-image: url('{{ asset('themeassets/img/backgrounds/login.jpg')}}');">
                                    	
                                    	
                                    	<h1>Let's  Get</h1>
                                        <h2 class="regh2">Started</h2>
                                    	<p id="regpara">Today we are thinking of all our members across the world and are encouraged by the support our networks bring in times such as these. Use the left test instrument details to attempt a transaction on the staging environment and  Register.</p>
                                        <h4>SIGN UP HERE</h4>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12 bgform">
                                   

                                    <form action="#" method="">

                                        <div class="login-form">
                                            <h4 class="login-title colorred">Register with you details in the below fields</h4>
                                           <hr>
                                            <div class="row">
                                                 <div class="col-md-6 col-12 mb-20">
                                                  
                                                    <input type="text" name="first_name"  value="" placeholder="&#xf007 &nbsp; First Name *">
                                                </div>
                                                <div class="col-md-6 col-12 mb-20">
                                                    
                                                    <input type="text" name="last_name" placeholder="Last Name *">
                                                </div>
                                                <div class="col-md-12 col-12">
                                                   
                                                    <input type="email" name="email" class="fas" 
                                                    placeholder="&#xf0e0 &nbsp; Email Address">
                                                </div>

                                                <div class="col-md-12 col-12">
                                                   
                                                    <input type="number" class="fas" 
                                                    placeholder="&#xf095 &nbsp; Phone Number">
                                                </div>

                                                <div class="col-md-6 col-12 mb-20">
                                                   
                                                    <input type="password" name="password" class="fas" placeholder=" &#xf023 &nbsp; Password">
                                                </div>

                                                <div class="col-md-6 col-12 mb-20">
                                                   
                                                    <input type="password" name="confimr_passowrd" class="fas" placeholder="&#xf13e &nbsp; Confirm Password">
                                                </div>

                                                 <hr style="width: 100%;margin-top: 5%;">
                                               
                                                     <div class="col-sm-12">
                                                 <h4 class="login-title colorred">Other Information</h4>
                                                        </div>
                                                  

                                                   <div class="col-md-12 col-12">
                                                   
                                                   <select class="form-control" name="category">
                                                       <option value="">Select Your Category</option>
                                                       <option value="dress">drees</option>
                                                       <option value="shirts">shirts</option>
                                                       <option value="pants">pants</option>
                                                  </select>
                                                </div>
                                                  

                                                   <div class="col-md-8 col-12 mb-20">
                                                  
                                                    <input type="text" name="gst_number"  value="" placeholder="Enter GST Number">
                                                </div>
                                                <div class="col-md-4 col-12 mb-20">
                                                    
                                                    <button class="btn btn-danger verifybtn">Verify Number</button>
                                                </div>

                                                <div class="col-md-6 col-12 mb-20">
                                                  
                                                    <input type="text" name="address1"  value="" placeholder="Address Line 1">
                                                </div>
                                                <div class="col-md-6 col-12 mb-20">
                                                    
                                                    <input type="text" name="apartment" placeholder="Apartment, Unit, Office">
                                                </div>
                                                

                                                 <div class="col-md-4 col-12 mb-20">
                                                  
                                                    <input type="text" name="city"  value="" placeholder="City">
                                                </div>
                                                <div class="col-md-4 col-12 mb-20">
                                                    
                                                      
                                                   <select class="form-control" name="state">
                                                       <option value="">State</option>
                                                       <option value="tamilnadu">Tamilnadu</option>
                                                       <option value="kerala">kerala</option>
                                                       <option value="andhara">andhara</option>
                                                  </select>
                                                </div>

                                              
                                              <div class="col-md-4 col-12 mb-20">
                                                    
                                                    <input type="text" name="zip_code" placeholder="Zip Code">
                                                </div>
                                                 

                                                  <div class="col-md-12 col-12">
                                                   
                                                   <select class="form-control" name="country">
                                                       <option value="">Country</option>
                                                       <option value="india">India</option>
                                                       <option value="china">China</option>
                                                       <option value="england">England</option>
                                                  </select>
                                                </div>
                                                  

                                                <div class="col-md-12">

                                                    <div class="check-box d-inline-block ml-0">
                                                        <input type="checkbox" id="remember_me" >
                                                        <label for="remember_me"style="font-style: normal;text-transform: lowercase;" >I need  agree to<i style="color:#05a6dc;font-size: 12px;font-weight: bolder;font-style: normal;"> Terms & conditions</i></label>
                                                    </div>

                                                </div>
                                                   <div class=" rs1 rs1b">
                                                     <button class="register-button btn-lg">SIGN UP</button>
                                                </div>
                                                   <p class="s1 rs1"><i> Have An Account?</i> <a href="{{url('/login')}}" > Please Sign up</a> </p>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--=======  End of page wrapper  =======-->
                </div>
            </div>
        </div>
</div>
@endsection

@section('front_script')

@endsection