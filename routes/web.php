<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
  Route::prefix('mills')->group(function () {
  	  Route::get('/mills_type', function () {
       return view('departments/mills/mills_type');
      });
      Route::get('/mills_company_req', function () {
       return view('departments/mills/mills_company_req');
      });
       Route::get('/mills_filter_list', function () {
       return view('departments/mills/mills_filter_list');
      });
        Route::get('/mills_post_req', function () {
       return view('departments/mills/mills_post_req');
      });
         Route::get('/f-fabric_type', function () {
       return view('departments/mills/fabric/fabric_type');
      });

         //fabric blends
        Route::get('/f-b-m-choose_melange_type', function () {
       return view('departments/mills/fabric/blends/melange_fabric/choose_melange_type');
      });
         Route::get('/f-b-m-fabric_req', function () {
       return view('departments/mills/fabric/blends/melange_fabric/fabric_req');
      });
        Route::get('/f-b-m-fabric_structure', function () {
       return view('departments/mills/fabric/blends/melange_fabric/fabric_structure');
      });
        Route::get('/f-b-r-choose_structure', function () {
       return view('departments/mills/fabric/blends/regular_fabric/choose_structure');
      });
        Route::get('/f-b-r-fabric_qty', function () {
       return view('departments/mills/fabric/blends/regular_fabric/fabric_qty');
      });
        Route::get('/f-b-r-fabric_req', function () {
       return view('departments/mills/fabric/blends/regular_fabric/fabric_req');
      });
         Route::get('/f-b-s-choose_structure', function () {
       return view('departments/mills/fabric/blends/special_fabric/choose_structure');
      });
          Route::get('/f-b-s-fabric_qty', function () {
       return view('departments/mills/fabric/blends/special_fabric/fabric_qty');
      });
        Route::get('/f-b-s-fabric_req', function () {
       return view('departments/mills/fabric/blends/special_fabric/fabric_req');
      });
        Route::get('/f-b-s-special_yarn_type', function () {
       return view('departments/mills/fabric/blends/special_fabric/special_yarn_type');
      });
        Route::get('/f-b-blends_fabric_choose', function () {
       return view('departments/mills/fabric/blends/blends_fabric_choose');
      });
        Route::get('/f-b-blends_fabric_type', function () {
       return view('departments/mills/fabric/blends/blends_fabric_type');
      });
        //fabric single
         Route::get('/f-s-m-choose_melange_type', function () {
       return view('departments/mills/fabric/single/melange_fabric/choose_melange_type');
      });
         Route::get('/f-s-m-fabric_req', function () {
       return view('departments/mills/fabric/single/melange_fabric/fabric_req');
      });
        Route::get('/f-s-m-fabric_structure', function () {
       return view('departments/mills/fabric/single/melange_fabric/fabric_structure');
      });
        Route::get('/f-s-r-choose_structure', function () {
       return view('departments/mills/fabric/single/regular_fabric/choose_structure');
      });
        Route::get('/f-s-r-fabric_qty', function () {
       return view('departments/mills/fabric/single/regular_fabric/fabric_qty');
      });
        Route::get('/f-s-r-fabric_req', function () {
       return view('departments/mills/fabric/single/regular_fabric/fabric_req');
      });
         Route::get('/f-s-s-choose_structure', function () {
       return view('departments/mills/fabric/single/special_fabric/choose_structure');
      });
          Route::get('/f-s-s-fabric_qty', function () {
       return view('departments/mills/fabric/single/special_fabric/fabric_qty');
      });
        Route::get('/f-s-s-fabric_req', function () {
       return view('departments/mills/fabric/single/special_fabric/fabric_req');
      });
        Route::get('/f-s-s-special_yarn_type', function () {
       return view('departments/mills/fabric/single/special_fabric/special_yarn_type');
      });
        Route::get('/f-s-single_fabric_choose', function () {
       return view('departments/mills/fabric/single/single_fabric_choose');
      });
        Route::get('/f-s-single_fabric_type', function () {
       return view('departments/mills/fabric/single/single_fabric_type');
      });
        //end fabric

         Route::get('/y-yarn_type', function () {
       return view('departments/mills/yarn/yarn_type');
      });
         //blends yarn
          Route::get('/y-b-d-yarn_qty', function () {
       return view('departments/mills/yarn/blends/dyed_yarn/yarn_qty');
      });
           Route::get('/y-b-d-yarn_qty_choose', function () {
       return view('departments/mills/yarn/blends/dyed_yarn/yarn_qty_choose');
      });
            Route::get('/y-b-m-melange_choose', function () {
       return view('departments/mills/yarn/blends/melange_yarn/melange_choose');
      });
            Route::get('/y-b-r-yarn_qty', function () {
       return view('departments/mills/yarn/blends/regular_yarn/yarn_qty');
      });
             Route::get('/y-b-s-yarn_qty', function () {
       return view('departments/mills/yarn/blends/special_yarn/yarn_qty');
      });
              Route::get('/y-b-s-special_yarn_type', function () {
       return view('departments/mills/yarn/blends/special_yarn/special_yarn_type');
      });
               Route::get('/y-b-blends_yarn_content', function () {
       return view('departments/mills/yarn/blends/blends_yarn_content');
      });
               Route::get('/y-b-blends_yarn_type', function () {
       return view('departments/mills/yarn/blends/blends_yarn_type');
      });
               //single yarn
                Route::get('/y-s-d-yarn_qty', function () {
       return view('departments/mills/yarn/single/dyed_yarn/yarn_qty');
      });
           Route::get('/y-s-d-yarn_qty_choose', function () {
       return view('departments/mills/yarn/single/dyed_yarn/yarn_qty_choose');
      });
            Route::get('/y-s-m-melange_choose', function () {
       return view('departments/mills/yarn/single/melange_yarn/melange_choose');
      });
            Route::get('/y-s-r-yarn_qty', function () {
       return view('departments/mills/yarn/single/regular_yarn/yarn_qty');
      });
      Route::get('/y-s-r-yarn_qty_count', function () {
         return view('departments/mills/yarn/single/regular_yarn/yarn_qty_count');
        });
             Route::get('/y-s-s-yarn_qty', function () {
       return view('departments/mills/yarn/single/special_yarn/yarn_qty');
      });
              Route::get('/y-s-s-special_yarn_type', function () {
       return view('departments/mills/yarn/single/special_yarn/special_yarn_type');
      });
               Route::get('/y-s-single_yarn_content', function () {
       return view('departments/mills/yarn/single/single_yarn_content');
      });
               Route::get('/y-s-single_yarn_type', function () {
       return view('departments/mills/yarn/single/single_yarn_type');
      });


  });
 Route::prefix('knitting')->group(function () {

   Route::get('/knitting_type', function () {
       return view('departments/knitting/knitting_type');
      });
      Route::get('/knitting_company_req', function () {
       return view('departments/knitting/knitting_company_req');
      });
       Route::get('/knitting_filter_list', function () {
       return view('departments/knitting/knitting_filter_list');
      });
        Route::get('/knitting_post_req', function () {
       return view('departments/knitting/knitting_post_req');
      });
         Route::get('/f-knitting_req', function () {
       return view('departments/knitting/flat/knitting_req');
      });
          Route::get('/f-knitting_structure', function () {
       return view('departments/knitting/flat/knitting_structure');
      });
           Route::get('/f-knitting_yarn_content', function () {
       return view('departments/knitting/flat/knitting_structure');
      });
           Route::get('/c-knitting_req', function () {
       return view('departments/knitting/circular/knitting_req');
      });
          Route::get('/c-knitting_structure', function () {
       return view('departments/knitting/circular/knitting_structure');
      });
           Route::get('/c-knitting_yarn_content', function () {
       return view('departments/knitting/circular/knitting_yarn_content');
      });
            Route::get('/c-knitting_yarn_qty', function () {
       return view('departments/knitting/circular/knitting_yarn_qty');
      });
             Route::get('/c-knitting_choose_count', function () {
       return view('departments/knitting/circular/knitting_choose_count');
      });
              Route::get('/c-knitting_choose_yarn', function () {
       return view('departments/knitting/circular/knitting_choose_yarn');
      });

 });
